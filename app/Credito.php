<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Credito extends Model
{
    //
    protected $fillable = array('solicitud_id', 'cooperativa_id', 'codigo_prestamo',
        'estado_prestamo','fecha_desembolso', 'fecha_pago','moneda','plazo','tiempo_gracia',
        'importe_credito','total_mes','tasa','interes','cuota_capital','saldo_capital',
        'suma_total_pagado','ultima_amortizacion');
    /**
     * Get the comments for the blog post.
     */
    public function plan()
    {
        return $this->hasMany('App\PlanCredito');
    }
    // TODO REMOVE DEPRECATED
    public function movimientos()
    {
        return $this->hasMany('App\MovimientoCredito');
    }
    public function movimientosD()
    {
        return $this->hasMany('App\MovimientoCredito');
    }
    public function movimientosA()
    {
        return $this->hasMany('App\MovimientoCredito');
    }

    public function amortizacion()
    {
        return $this->hasMany('App\Amortizacion');
    }
    public function desembolso()
    {
        return $this->hasMany('App\Desembolso');
    }
    public static function createNewPlanCredito($saldo_capital, $plazo, $gracia, $interesAnual, $fecha_ini, $diaPago, $i = 1)
    {
        // Generate Interes
        // Mensual: anual/mesesAño/porcentaje
        $interes_mensual = $interesAnual/12/100;
        // Diario: Mensual/30Dias
        $interesDiario = $interes_mensual/30;

        // Generate Dates TODO Agregar DiaPago
        $start = new DateTime($fecha_ini);
        $year = $start->format('Y');
        $month = $start->format('m');
        $day = $start->format('d');
        $finishDate = new DateTime($fecha_ini);
        $finishDate->setDate($year, $month + $plazo,$diaPago);
        //return $start->format('Y-m-d');
        //return $finishDate->format('Y-m-d');

        // Main loop
        $tempOldDate = new DateTime($fecha_ini);
        $mainPC = [];
        $cuotaCapitalMes = $saldo_capital / ($plazo - $gracia);
        $saldoCapital = $saldo_capital;
        for(; $i<=$plazo; $i++){
            $mainPC[$i]['cuota'] = $i;
            $tempNewDate = new DateTime();
            $tempNewDate->setDate($year,$month+$i,$diaPago);
            $mainPC[$i]['fechaInicio'] = $tempOldDate->format('Y-m-d');
            $mainPC[$i]['fechaPago'] = $tempNewDate->format('Y-m-d');
            $tempDateDiff = date_diff($tempOldDate,$tempNewDate, '%d')->days;
            if($i<=$gracia){
                $mainPC[$i]['capitalAmortizar'] = 0;
                $mainPC[$i]['periodoGracia'] = 0;
            }
            else {
                $mainPC[$i]['capitalAmortizar'] = round($cuotaCapitalMes,2);
                $mainPC[$i]['periodoGracia'] = 1;
            }
            $mainPC[$i]['interesSaldos'] = round($saldoCapital*$interesDiario*$tempDateDiff, 2);
            $saldoCapital -= $mainPC[$i]['capitalAmortizar'];
            $mainPC[$i]['cuotaTotalMes'] = round($mainPC[$i]['capitalAmortizar'] + $mainPC[$i]['interesSaldos'], 2);
            $mainPC[$i]['saldoCapital'] = round($saldoCapital, 2);
            $tempOldDate = new DateTime();
            $tempOldDate->setDate($year,$month+$i,$diaPago);
        }
        return $mainPC;
    }

    public function terminarCredito () {
        $sum = 0;
        $amortizacion = $this->amortizacion();
        foreach ($amortizacion as $row) {
            $sum = $sum+ $row['importe'];
        }
        if ($sum == $this->importe_credito){
            return "credito completado";
        }
        else {
            return "Credito por completar";
        }
    }
}

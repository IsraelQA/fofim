<?php

namespace App;

class Helper
{
    //
    public function createNewPlanCredito($saldo_capital, $plazo, $gracia, $interesAnual, $fecha_ini, $diaPago, $i = 1)
    {
        // Generate Interes
        // Mensual: anual/mesesAño/porcentaje
        $interes_mensual = $interesAnual/12/100;
        // Diario: Mensual/30Dias
        $interesDiario = $interes_mensual/30;

        // Generate Dates TODO Agregar DiaPago
        $start = new DateTime($fecha_ini);
        $year = $start->format('Y');
        $month = $start->format('m');
        $day = $start->format('d');
        $finishDate = new DateTime($fecha_ini);
        $finishDate->setDate($year, $month + $plazo,$diaPago);
        //return $start->format('Y-m-d');
        //return $finishDate->format('Y-m-d');

        // Main loop
        $tempOldDate = new DateTime($fecha_ini);
        $mainPC = [];
        $cuotaCapitalMes = $saldo_capital / ($plazo - $gracia);
        $saldoCapital = $saldo_capital;
        for(; $i<=$plazo; $i++){
            $tempNewDate = new DateTime();
            $tempNewDate->setDate($year,$month+$i,$diaPago);
            $mainPC[$i]['fechaInicio'] = $tempOldDate->format('Y-m-d');
            $mainPC[$i]['fechaPago'] = $tempNewDate->format('Y-m-d');
            $tempDateDiff = date_diff($tempOldDate,$tempNewDate, '%d')->days;
            if($i<=$gracia){
                $mainPC[$i]['capitalAmortizar'] = 0;
            }
            else {
                $mainPC[$i]['capitalAmortizar'] = round($cuotaCapitalMes,2);
            }
            $mainPC[$i]['interesSaldos'] = round($saldoCapital*$interesDiario*$tempDateDiff, 2);
            $saldoCapital -= $mainPC[$i]['capitalAmortizar'];
            $mainPC[$i]['cuotaTotalMes'] = round($mainPC[$i]['capitalAmortizar'] + $mainPC[$i]['interesSaldos'], 2);
            $mainPC[$i]['saldoCapital'] = round($saldoCapital, 2);
            $tempOldDate = new DateTime();
            $tempOldDate->setDate($year,$month+$i,$diaPago);
        }
        return $mainPC;
    }
}

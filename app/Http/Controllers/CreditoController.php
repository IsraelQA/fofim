<?php

namespace App\Http\Controllers;

use App\Credito;
use App\Solicitud;
use App\Cooperativa;
use App\TipoPrestamo;
use App\ResolucionSolicitud;
use App\PlanCredito;
use App\MovimientoCredito;
use App\Amortizacion;
use App\Desembolso;

use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\Http\Requests;

class CreditoController extends Controller
{

    /**
     * List of to-dos
     * Remap Desembolsos
     * Create new views for amortizaciones
     * Create controller for amortizaciones
     * End a credit
     * Update mainscreen with realdata values
     * Make a report
     * Include delete action on controller
     * Include delete action on view
     *
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cooperativas = Cooperativa::orderBy('created_at', 'asc')->get()->lists('nombre', 'id');
//        $tipo_prestamos = TipoPrestamo::orderBy('created_at', 'asc')->get()->lists('nombre', 'id');
        $creditos = Credito::orderBy('created_at', 'asc')->get();

        return view('credito.list', [
            'creditos' => $creditos,
//            'tipo_prestamos' => $tipo_prestamos,
            'cooperativas' => $cooperativas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'solicitud_id' => 'required',
//            'codigo_prestamo' => 'unique:creditos|required',
            'codigo_prestamo' => 'required',
            'fecha_aprobacion' => 'required',
            'fecha_desembolso' => 'required',
            'fecha_pago' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator);
        }

        $id = $request->solicitud_id;
        $solicitud = Solicitud::findOrFail($id);

        // ESTADOS.- 0. pendiente, 1. aprobada, 2.rechazada
        if ($solicitud->estado >= 1) {
            return Redirect::back()
                ->withInput()
                ->withErrors(array("La solicitud ya fue aprobada, contacte al administrador del sistema."));
//            return "Error. La solicitud ya fue aprobada, contacte al administrador del sistema.";
        }


        $tipo_prestamo = TipoPrestamo::findOrFail($solicitud->tipo_credito_id);
        $cooperativa = Cooperativa::findOrFail($solicitud->cooperativa_id);


        $saldo_capital = $solicitud->importe_solicitado;
        $plazo = $tipo_prestamo->tiempo_maximo_pago;
        $gracia = $tipo_prestamo->tiempo_de_gracia;
        $interes_anual = $tipo_prestamo->interes;
        //la fecha de inicio comienza al primer desembolso
        $fecha_ini = $request->fecha_desembolso;
        $fecha_pago = $request->fecha_pago;

        $detalle_plan = Credito::createNewPlanCredito($saldo_capital, $plazo, $gracia, $interes_anual, $fecha_ini, $fecha_pago);

        //  Old Plan TODO Remove this
        //$detalle_plan = $this->createPlanCredito($saldo_capital, $plazo, $gracia, $interes_anual, $fecha_ini);

        $resolucion = new ResolucionSolicitud();
        $resolucion->fill(array('solicitud_id' => $solicitud->id, 'estado' => 1,
            "fecha_resolucion" => $request->fecha_aprobacion, 'monto_aprobado' => $solicitud->importe_solicitado));
        // Start Debugging here
        //return $resolucion;
        $resolucion->save();
        $solicitud->estado = 1;
        $solicitud->save();

//      Creando el nuevo credito

        $credito = new Credito();
        $data = ['solicitud_id' => $solicitud->id,
            'cooperativa_id' => $solicitud->cooperativa_id,
            'codigo_prestamo' => $request->codigo_prestamo,
            //Estado del Prestamo: 0. pendiente, 1. Finalizado, 2. Otros
            'estado_prestamo' => 0,
            'fecha_desembolso' => $fecha_ini,
            'fecha_pago' => $request->fecha_pago,
            'moneda' => 'Bolivianos',
            'plazo' => $plazo,
            'tiempo_gracia' => $gracia,
            'importe_credito' => $saldo_capital,
            'interes' => $interes_anual,
            'saldo_capital' => $saldo_capital,
            'suma_total_pagado' => 0,

        ];
        $credito->fill($data);
        $credito->save();

//        return $detalle_plan;
        // todo llenando plan de creditos

        foreach ($detalle_plan as $row) {
            $plan_row = new PlanCredito();
            $plan_row->credito_id = $credito->id;
            $plan_row->periodo_gracia = $row['periodoGracia'];
            $plan_row->fecha_pago = $row['fechaPago'];
            $plan_row->cuota_capital = $row['capitalAmortizar'];
            $plan_row->cuota_interes = $row['interesSaldos'];
            $plan_row->total_cuota = $row['cuotaTotalMes'];
            $plan_row->saldo_capital = $row['saldoCapital'];
            $plan_row->save();
//            return $plan_row;
        }

        return redirect("/credito/" . $credito->id);
//        return "Solicitud Correctamente Aprobada";
//        $value = new Solicitud();
//        $input = $request->all();
//        $value->fill($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $credito = Credito::findOrFail($id);
        $plan = $credito->plan;
        // todo Update according
        //$desembolso = $credito->movimientos()->where('tipo', 0)->get();
        //$amortizacion = $credito->movimientos()->where('tipo', 1)->get();
        $desembolso = $credito->desembolso;
        $amortizacion = $credito->amortizacion;
        $cooperativa = Cooperativa::findOrFail($credito->cooperativa_id);
        $solicitud = Solicitud::findOrFail($credito->solicitud_id);
        $sum = array();
//return $solicitud;
//        Sum amortizaciones
        $sumAmortizacion = 0;
        if (!empty($amortizacion)) {
            foreach ($amortizacion as $row) {
                $sumAmortizacion = $sumAmortizacion + $row['importe'];
            }
        }
        $sum['amortizacion'] = $sumAmortizacion;

//        Sum desembolsos
        $sumDesembolsos = 0;
        if (!empty($desembolso)) {
            foreach ($desembolso as $row) {
                $sumDesembolsos = $sumDesembolsos + $row['importe'];
            }
        }
        $sum['desembolso'] = $sumDesembolsos;

//        return $credito;
//        return $plan;


//        $saldo_capital=$solicitud->importe_solicitado;
//        $plazo=$tipo_prestamo->tiempo_maximo_pago;
//        $gracia=$tipo_prestamo->tiempo_de_gracia;
//        $interes_anual=$tipo_prestamo->interes;
//        $fecha_ini=$solicitud->fecha_solicitud;

        return view('credito.show',
            ['cooperativa' => $cooperativa,
                'solicitud' => $solicitud,
                'amortizacion' => $amortizacion,
                'desembolso' => $desembolso,
                'sum' => $sum,
                'detalle_plan' => $plan])->withCredito($credito);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function editPlanCredito(){
        $data = $this->testPlanCredito();
//        return $data;
        return view('credito.plan', ['data' => $data]);
    }

    public function updatePlanCredito(){
        $data = $this->testPlanCredito();
//        return $data;
        return view('credito.plan', ['data' => $data]);
    }

    public function testPlanCredito2(){
        $data = $this->testPlanCredito();
//        return $data;
        return view('credito.plan', ['data' => $data]);
    }


    public function testPlanCredito() {
        // Pruebas Plan
        $monto_prestamo = 376380;
        $interes_anual = 6;
        $plazo = 36;
        $gracia = 3;
        $fecha_ini = "2016-03-23";
        // Nueva Variable: Dia Pago
        $diaPago = 29;
        $PC1 = Credito::createNewPlanCredito($monto_prestamo, $plazo, $gracia, $interes_anual, $fecha_ini, $diaPago,1);

        $monto_prestamo = 418200;
        $interes_anual = 6;
        $plazo = 31;
        $gracia = 0;
        $fecha_ini = "2016-09-08";
        // Nueva Variable: Dia Pago
        $diaPago = 29;
        $PC2 = Credito::createNewPlanCredito($monto_prestamo, $plazo, $gracia, $interes_anual, $fecha_ini, $diaPago);
        return $PC2;
        //return $PC1;
    }

    public function testPlanCreditoOriginal()
    {
        // Pruebas Plan
        $monto_prestamo = 800000;
        $interes_anual = 6;
        $plazo = 36;
        $gracia = 3;
        $fecha_ini = "2015-12-29";
        // Nueva Variable: Dia Pago
        $diaPago = 29;
        $PC1 = Credito::createNewPlanCredito($monto_prestamo, $plazo, $gracia, $interes_anual, $fecha_ini, $diaPago);
        return $PC1;

        // if going to add to movimientos
        $testPlan = new MovimientoCredito(['tipo' => '0']);

        return $testPlan;
    }

    // Deprecated
    public function createPlanCredito($saldo_capital, $plazo, $gracia, $interes_anual, $fecha_ini)
    {

        $interes_mensual = $interes_anual / 12 / 100;

        $cuota_capital_mes = round($saldo_capital / ($plazo - $gracia), 2);

        $contador_mes = 0;
        // Primer Mes diferencia
        $date = date('Y-m-d', strtotime($fecha_ini));
        $date = date('Y-m-d', strtotime($date . ' + 1 month'));
        $detalle_plan = [];
        while ($contador_mes < $plazo) {
            $detalle_plan[$contador_mes]['fecha'] = $date;

            $date = date('Y-m-d', strtotime($date . ' + 30 days'));

            if ($contador_mes < $gracia) {
                $detalle_plan[$contador_mes]['periodo_gracia'] = 0;
                $detalle_plan[$contador_mes]['cuota_mes'] = 0;
                $cuota_interes = round($saldo_capital * $interes_mensual, 2);
                $detalle_plan[$contador_mes]['cuota_interes'] = $cuota_interes;
                $cuota_total = 0 + $cuota_interes;
                $detalle_plan[$contador_mes]['cuota_total'] = $cuota_total;
                $detalle_plan[$contador_mes]['saldo_capital'] = $saldo_capital;
            } else {
                $detalle_plan[$contador_mes]['periodo_gracia'] = 1;
                $detalle_plan[$contador_mes]['cuota_mes'] = $cuota_capital_mes;
                $cuota_interes = round($saldo_capital * $interes_mensual, 2);
                $detalle_plan[$contador_mes]['cuota_interes'] = $cuota_interes;
                $cuota_total = $cuota_capital_mes + $cuota_interes;
                $detalle_plan[$contador_mes]['cuota_total'] = $cuota_total;
                $saldo_capital = $saldo_capital - $cuota_capital_mes;
                if ($saldo_capital >= 0) {
                    $detalle_plan[$contador_mes]['saldo_capital'] = $saldo_capital;
                } else {
                    $detalle_plan[$contador_mes]['saldo_capital'] = 0;
                }

            }
            $contador_mes = $contador_mes + 1;

        }
        return $detalle_plan;
    }
}

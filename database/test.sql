SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


INSERT INTO `cooperativas` (`id`, `nombre`, `personeria_juridica`, `codigo`, `nro_registro`, `derecho_consesionario`, `federacion_afiliada`, `ci_representante_legal`, `nombre_representante_legal`, `cantidad_socios`, `fecha_formacion`, `fecha_resolucion`, `direccion`, `departamento_id`, `provincia_id`, `municipio_id`, `localidad_id`, `telefono`, `fax`, `casilla_postal`, `email`, `web`, `coordinadas_utm`, `produccion_anual`, `mineral_id`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'Cooperativa  Minera Aurifera Yuna Ltda.', '123', 'COOP-YUNA', '1', '2', '3', '4', 'Placido Garcia Montecinos    ', 0, '2017-11-15', '2017-11-12', 'a', 0, 0, 0, 0, '2', '3', '', '', '', 'v', '', 1, '0.0000000000', '0.0000000000', '2017-11-15 13:06:53', '2017-11-15 13:46:30');

INSERT INTO `mineral_produccion` (`id`, `nombre`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'Aurifera', 'Aurifera', '2017-11-15 13:01:46', '2017-11-15 13:01:46');

INSERT INTO `solicitudes` (`id`, `cooperativa_id`, `tipo_credito_id`, `nro_solicitud`, `nombre_proyecto`, `nombre_proyectista`, `fecha_solicitud`, `importe_solicitado`, `importe_propio`, `importe_total`, `socio_id`, `estado`, `objeto_prestamo`, `licencia_ambiental`, `federacion_afiliacion`, `adjunto`, `requisitos`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '018/2015', 'Declaracion Jurada"Mejora en la Explotacion Aurifera Tajo de Yuna"', 'Placido Garcia Montecinos', '2015-10-12', 800000, 0, 800000, 0, 0, '0', '123', '321', '', '', '2017-11-15 14:22:11', '2017-11-15 14:22:11');

INSERT INTO `tipo_prestamo` (`id`, `nombre`, `minimo`, `maximo`, `tiempo_de_gracia`, `tiempo_maximo_pago`, `interes`, `comisiones`, `created_at`, `updated_at`) VALUES
(1, '[Bs. 800.000] Proyectos destinados a la mejora de sistemas de explotación y concentración de minerales', 1, 800000, 3, 36, 6, 0, '2017-09-24 19:38:12', '2017-09-24 19:38:12');

INSERT INTO `users` (`id`, `name`, `email`, `password`, `group`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@change.me', '$2y$10$w/UaHkWVFgV/kM05b/0l4uMj0lYo8C3fAqKgkl.aoL7G2YtjqWlGa', '', '', 'LY58d83WT2YgYa3aoU4xVkH6gbxgydk8PIyG8y6xds1mrlrNEtrwGIE9ADJB', '2017-08-14 08:02:17', '2017-09-06 15:48:28');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Realizar Amortizacion</b></h3>
                    <div class="box-tools pull-right">
                        {{--                        <a href="{{route('credito.edit', $credito->id)}}" class="btn btn-sm btn-primary btn-flat pull-left"><i class='fa fa-edit'></i> </a>--}}
                        <a href="{{url('credito')}}" class="btn btn-sm btn-info btn-flat pull-left"><i class='fa fa-th-large'></i> Lista</a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('layouts.partials.errors')
                    {{--array('credito_id','plan_credito_id','fecha_pago','documento','importe','nombre_depositante',--}}
                    {{--'ci','adjunto','observacion');--}}
                    <p>Realiza las amortizaciones en este formulario</p>
                    {!! Form::open(array('url' => 'amortizacion')) !!}
                    {!! Form::hidden('credito_id', $credito->id) !!}
                    <div class="form-group">
                        {!! Form::label('codigo_prestamo', 'Codigo Credito:', ['class' => 'control-label']) !!}
                        {!! Form::text('codigo_prestamo', $credito->codigo_prestamo, ['class' => 'form-control', 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('documento', 'Identificador del Documento:', ['class' => 'control-label']) !!}
                        {!! Form::text('documento', '', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('importe', 'Importe:', ['class' => 'control-label']) !!}
                        {!! Form::text('importe', 0, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('fecha_pago', 'Fecha Pago:', ['class' => 'control-label']) !!}
                        {!! Form::input('date', 'fecha_pago', date('d-m-Y'), ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('nombre_depositante', 'Nombre Completo Depositante:', ['class' => 'control-label']) !!}
                        {!! Form::text('nombre_depositante', '', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ci', 'C.I. Depositante:', ['class' => 'control-label']) !!}
                        {!! Form::text('ci', '', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('observacion', 'Observaciones:', ['class' => 'control-label']) !!}
                        {!! Form::text('observacion', '', ['class' => 'form-control']) !!}
                    </div>

                    <!-- Add Button -->
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-plus"></i> Agregar Amortizacion
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
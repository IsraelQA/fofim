@extends('layouts.app')

@section('contentheader_title')
	Plan de Creditos
@endsection
@section('htmlheader_title')
	Crear
@endsection


@section('main-content')
<div class="row">
        <div class="col-xs-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Crear plan de Creditos</h3>
              <div class="box-tools pull-right">
                            <a href="{{url('test2')}}" class="btn btn-sm btn-info btn-flat pull-left"><i class='fa fa-th-large'></i> Lista</a>

                            </div>
            </div><!-- /.box-header -->
            <div class="box-body">
<!-- New Task Form -->
        @include('layouts.partials.errors')
                {!! Form::open(array('url' => 'test2')) !!}
                {!! csrf_field() !!}
<div class="box-body table-responsive no-padding">
    <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-info"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Crea el Plan de Creditos</span>
              <span class="info-box-number">Ajusta considerando fines de semana y dias festivos</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
              <span class="progress-description">
                    Una vez realizado el ajuste selecciona "Añadir Plan de Creditos"
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
              <table class="table table-hover">
                <tbody><tr>
                  <th>Cuota N°</th>
                  <th>Fecha de Inicio</th>
                  <th>Fecha de Pago</th>
                  <th>Capital a amortizar</th>
                  <th>Interes</th>
                  <th>Cuota Total</th>
                  <th>Saldo Capital</th>
                </tr>
@foreach ($data as $key => $row)
                <tr>
                  <td>{{ $row['cuota'] }}</td>
                  <td>{{ Form::date('fechaInicio_'.$key, $row['fechaInicio'], ['class' => 'form-control']) }}</td>
                  <td>{{ Form::date('fechaPago_'.$key, $row['fechaPago'], ['class' => 'form-control']) }}</td>
                  <td>{{ $row['capitalAmortizar'] }}</td>
                  <td>{{ $row['interesSaldos'] }}</td>
                  <td>{{ $row['cuotaTotalMes']}}</td>
                  <td>{{ $row['saldoCapital']}}</td>
                </tr>
@endforeach
                
              </tbody></table>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-5 col-sm-6">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Añadir Plan de Creditos
                    </button>
                </div>
            </div>

        {!! Form::close() !!}

            </div><!-- /.box-body -->
          </div><!-- /.box -->

        </div>
        <!-- /.col -->
      </div>

@endsection
{{--@push('pagescripts')--}}
{{--<script>--}}
{{--$(document).ready(function() {--}}
    {{--$('#example').DataTable();--}}
{{--} );--}}
{{--</script>--}}
{{--@endpush--}}